### Assignment 2, opgave 2 - Clothing level

setwd("~/Documents/DTU/2. Semester (MSc)/ASM/02424_AdvStatMod/Assignment2")
D = read.csv('dat_count.csv',  sep=";", stringsAsFactors = TRUE)
str(D)
D$subjId <- as.factor(D$subjId)
n = dim(D)[1]
p = dim(D)[2]
summary(D)


######################
#### 0. Data viz
plot(D[,3:p])

###########################
##### 1. GLM - Binomial
library('MASS')
binom = glm(cbind(clo, D$nobs - D$clo) ~ (time + sex + tOut + tInOp)^2 + I(tInOp^2) + I(tOut^2), # nobs - clo = antal ikke-skift
                   family = "binomial", #logit as canonical 
                   data=D)
summary(binom)

  # Reduce
          #binom2 <- step(binom, test="Chisq") # Hov, den bruger vidst stadig AIC
drop1(binom, test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2)), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut -time:tInOp), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut -time:tInOp -time:sex), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut -time:tInOp -time:sex -time), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut -time:tInOp -time:sex -time -sex:tInOp), test="Chisq")
drop1(update(binom, ~. -I(tOut^2) -I(tInOp^2) -tOut:tInOp -sex:tOut -time:tOut -tOut -time:tInOp -time:sex -time -sex:tInOp -tInOp), test="Chisq")


binomred = glm(cbind(clo, D$nobs - D$clo) ~ sex,
               family = "binomial",
               data = D)
summary(binomred)
library("knitr")
library("xtable")
print(xtable(binomred, type = "latex"))


binomslightreduced = glm(cbind(clo, D$nobs - D$clo) ~ time + sex + tInOp + time:sex + 
                           sex:tInOp,
                        family = "binomial",
                        data = D)
summary(binomslightreduced)

  # Plot
par(mfrow=c(2,2))
plot(binomred)  ; par(mfrow=c(1,1))
#MANGLER: Cox-Snell i stedet for "Normal Q-Q"????

  # Plot against explaining variables
plot(binomred$residuals ~ D$sex) # Foreslå weighted analysis, dont do it.
plot(rstandard(binomred) ~ D$sex)
plot(residuals(binomred, type="deviance") ~ D$sex) #Deviance residuals

# Overdispersion?
1 - pchisq(deviance(binomred), 134)
# JA, håndtér:
qbinomred = glm(cbind(clo, D$nobs - D$clo) ~ sex,
               family = "quasibinomial",
               data = D)
summary(qbinomred) # p. 95/90- 1/dispersion par = precision
print(xtable(summary(qbinomred), "latex"))
plot(qbinomred)

1 - pf(deviance(qbinomred), 136-1, 136-1) # MANGLER, Korrekt F-test, dispersion-par???
  # F-test til model red., ikke goodness-of-fit

## Tjek GAM
library(mgcv); par(mfrow=c(1,2))
model<-gam(clo ~ s(tInOp) + s(tOut), data=D)
summary(model)
plot(model)
## Virker overfittet



###########################
###### 2. GLM - Poisson  # Mangler: offset, wtf? Offset: Counts -> Ratio, at clo er proportional med nobs.
pois = glm(clo ~ time + offset(log(nobs)) + sex + tOut + tInOp + sex:(tOut + tInOp) + I(tInOp^2)+ I(tOut^2),
              family = "poisson",      #log() as canonical <> ensuring positive integers
              data=D)
summary(pois)

# Reduce
drop1(pois, test="Chisq")
drop1(update(pois, ~ . -I(tOut^2)), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2)), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2) -time), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2) -time -tOut), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2) -time -tOut -tInOp), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2) -time -tOut -tInOp -sex:tOut), test="Chisq")
drop1(update(pois, ~ . -I(tOut^2) -I(tInOp^2) -time -tOut -tInOp -sex:tOut -sex:tInOp), test="Chisq")

poisred = glm(clo ~ offset(nobs) + sex, family = "poisson", data=D)
summary(poisred)
print(xtable(poisred, "latex"))

# Plot fit
par(mfrow=c(2,2)); plot(poisred)

### Overdispersion (p. 112)?
  # Numerisk
summary(poisred)
1 - pchisq(deviance(poisred), 134)
# Vi kan ikke forkaste, at the residual deviance er naturligt for modellen, AKA: ingen over/under dispersion



###########################
###### 3. Interpretation
# Binomial virker mere som den fede: clo ~ Binom(p_i, nobs)
# hvor p_i = f(x) = sands. for at skifte have skiftet clothinglevel ved en given obs.

# Kør gerne AIC comparison (for non-nested models)
# NA for quasibinomial (MANGLER: Hvorfor?). For alm. vinder Poisson over Binom på AIC

###########################
###### 4. In conclusion
# There are multiple ways to model this count data. Binom er måske den fede





### MANGLER:
# -1 "Offset" i Poisson - se p. 123
# (1. Weighted analysis? Nej - evt. forslå fra residual plot, men udgør ikke.)

