## Assignment 1 - David

########################
########################
# A
setwd("~/Documents/DTU/2. Semester (MSc)/Adv. Statistical Modelling/02424_AdvStatMod")
D = read.csv('clothingSum.csv')
D$sex = as.factor(D$sex)
D$subjId = as.factor(D$subjId)
D$day = as.factor(D$day)
summary(D)

######## 1. Data Viz
## Scatters
plot(D[3:6]) # Highly correlated variables (multicollinearity!)
cor(D[3:5]) # Correlations

## Histograms
hist(D$clo, breaks =  10) # Normal between [0, 1] (intrinsicly descrete)
hist(D$tOut, breaks = 40)  # Mixture Models with m=2 Normal
hist(D$tInOp) # Normal w/ left-tail
D$female = as.numeric(D$sex == "female")
hist(D$female) # Uniform
str(D)

## Pairwise visualisations
pairs(~clo+tOut+tInOp, data=D, col = (D$sex))   # Mænd er rød

plot(clo~sex, data = D, col=c(2,3))


######## A.2 + A.3). General Liner Models
  # 2.1 Initial fit
fit = lm(clo ~ tOut * tInOp, data = D)
summary(fit) # Yikes, let's overwrite it imidiatly:
fit = lm(clo ~ tOut + tInOp, data = D)
summary(fit)
  # Since D$clo is [0, 1] we should use logistic regression or similar transformations, to ensure
  # y_hat = [0, 1]:
  # clo = 1/(1+exp(clo_transformed))
D$clo_t = log(1/D$clo - 1)  # clo_t has been isolated from the above

  # test the backtransform:
round(D$clo, digits=12) == round(1/(1+exp(D$clo_t)) , digits = 12)

  # 2.2 Fit - transformed clo
fit1 = lm(clo_t ~ tOut + tInOp, data = D)
summary(fit1)
par(mfrow = c(2,2));   plot(fit1);  par(mfrow = c(1,1)); # Graphical Representation
confint(fit1)                                            # Confidence Intervals
# Consider making splits on the tOut
# Consider removing tInOp:
fit2 = lm(clo_t ~ tOut, data = D)
summary(fit2)

  # 2.3 Type III selection
library('car')
anova(fit2,fit1) # ikke signifikant forskel
Anova(fit1,fit2, type = "III")


  # 2.4 gam()
library(mgcv)
par(mfrow=c(2,1))
model<-gam(clo~s(tOut)+s(tInOp),data=D)
summary(model)
plot(model)
    # Suggests a 3-order polynomial fit of tInOp to explain clo - too overfittet imo.


#####
  # A.4 + A.5)
fit2 = lm(clo_t ~ (tOut + tInOp) * sex, data = D)
summary(fit2)
fit2 = lm(clo_t ~ tOut + tInOp + sex + tInOp*sex, data = D)
summary(fit2)
confint(fit2)
par(mfrow = c(2,2));   plot(fit2);  par(mfrow = c(1,1)); # Graphical Representation
  # Again, residuals are super OFF of the normality assumption. Now even worse.
hist(residuals(fit2),20)

  # A.6)
boxplot(fit2$residuals ~ D$subjId)
    # Seems to be high difference distribution of residuals per subjet =>
    # include subjectID



###########################################################################
########## Problem B: Including subjectID ################################
###########################################################################
fit3 = lm(clo_t ~ tOut + tInOp + sex + tInOp:sex + subjId, data = D)
summary(fit3)
confint(fit3)
par(mfrow = c(2,2));   plot(fit3);  par(mfrow = c(1,1)); # Graphical Representation
  # Much better QQPlot
  # For the studentized residuals:
qqnorm(rstudent(fit3)); qqline(rstudent(fit3)); # Seems OK

# Is it better?
anova(fit2,fit3)
  # Yes

######### B.2
plot(confint(fit3)) # Nogenlunde visual presentation.


######### B.3
# bla. bla. bla. koefficienter er ændring i y ved én ændring i x. Lineær/additiv effekt.
# The model can only be used in predicting clothing level for the SAME subjects.
# in predicting a new subject, a simper model (without subjId) would have to be used.

###########################################################################
########## Problem C: Full data set #######################################
###########################################################################
D1 = read.csv('clothingFull.csv')
D1 = D1[,-1]
D1$sex = as.factor(D1$sex)
D1$subjId = as.factor(D1$subjId)
D1$day = as.factor(D1$day)
dim(D1)
summary(D1)
D1$clo_t = log(1/D1$clo - 1) # transform

###### Fit
fit4 = lm(clo_t ~ tOut + tInOp + sex + tInOp:sex + subjId + obs.no, data = D1)
summary(fit4)
confint(fit4)
par(mfrow = c(2,2));   plot(fit4);  par(mfrow = c(1,1));

#### Re-fit
fit4b = step(fit4,~+1, direction = 'backward')
summary(fit4b)

  # Is it better?
anova(fit4b, fit4) # they are the same


#### Comments
# Independance: Violated, since there are 6 observations per person per day. Because people
#   typically wear the same clothes throughout the day, knowing one observations is extremely
#   informational on the other observations that same day <=> NOT INDEPENDENCE

# Normal distribution: Deviates in the tails of the QQplot.

#### Notes for report:
# 1. Math formula (of GLMs), not R
# 2. Confidence Intervals & Plots

#### Spørgsmål til Jan/Jannick:
# 1. Reducér subjId for non-signifikante?
# 2. Visualisér parametre
# 3. Backward Selection? Type 3: anova(fit0, fit1)