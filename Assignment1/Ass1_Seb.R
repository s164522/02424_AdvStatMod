library('car')
library('ggplot2')
library('GGally')
library('dplyr')
library('ggfortify')

## Assignment 1 - Seb

# A
#1
setwd("/Users/Admin/Documents/DTU/Advanced Dataanalysis and Statistical modelling/02424_AdvStatMod")
CS = read.csv('clothingSum.csv')
CS$female = as.numeric(CS$sex == "male")
CS$sex = as.factor(CS$sex)
CS$subjId = as.factor(CS$subjId)
CS$day = as.factor(CS$day)

summary(CS)
head(CS)
ggpairs(CS[3:6])
#multicollinearity

#hist(CS$clo, breaks = 10) #positive skew, looks gaussian
#hist(CS$tOut, breaks = 40)  # bimodal
#hist(CS$tInOp, breaks=6) # negative skew, looks gaussian

p<-ggplot(CS, aes(x=clo, color=subjId)) + geom_boxplot()
p

#2
fit = lm(clo ~ tOut + tInOp, data = CS)
summary(fit)
autoplot(fit)
#Maybe logit transform?
#CS$clo_logit = log(1/CS$clo - 1)
CS$clo_logit = logit(CS$clo)
#near(CS$clo_logit, CS$clo_logit2, tol = .Machine$double.eps^0.5)

fit = lm(clo_logit ~ tOut + tInOp, data = CS)
summary(fit)
autoplot(fit)

step(fit, direction = "backward")

min_fit = lm(clo_logit ~ tOut, data = CS)

step(min_fit, direction = "forward", scope = formula(fit))

#4
fit2 = lm(clo_logit ~ tOut + tInOp + sex + tInOp:sex, data = CS)
summary(fit2)
autoplot(fit2)
confint(fit2)

step(fit, direction = "backward")

min_fit2 = lm(clo_logit ~ tOut, data = CS)

step(min_fit, direction = "forward", scope = formula(fit))


#6
p<-ggplot(CS, aes(x=fit2$residuals, color=subjId)) + geom_boxplot()
p



#B
fit3 = lm(clo_logit ~ tOut + tInOp + sex + tInOp:sex + subjId, data = CS)
summary(fit3)
confint(fit3)
autoplot(fit3)

#C
CS = read.csv('clothingFull.csv')
